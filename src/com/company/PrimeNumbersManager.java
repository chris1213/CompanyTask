package com.company;

import java.util.ArrayList;

public class PrimeNumbersManager  {

    private ArrayList<Long> primeList;
    private ArrayList<Model> palindromList;

    public void primeBasicMethod(){
        primeList = new ArrayList<>();
        palindromList = new ArrayList<>();

        findPrimeNumbers();
        savingPalindromOnList();
        sortElements(palindromList);
        showPalindrom();
    }

    private void findPrimeNumbers() {
        int number;
        for (number = 10000; number <= 99999; number++) {
            if (isPrime(number)) {
                addPrimeNumberToList(number);
            }
        }
    }

    private void savingPalindromOnList() {
        int firstAttribute;
        int secondAttribute;
        long result;

        for (firstAttribute = primeList.size() - 1; firstAttribute >= 0; firstAttribute--) {

            for (secondAttribute = primeList.size() - 1; secondAttribute >= 0; secondAttribute--) {
                long firstA = primeList.get(firstAttribute);
                long secondA = primeList.get(secondAttribute);
                result = firstA * secondA;
                if (isPalindrome(result)) {
                    addPalindromNumberToList(result, firstA, secondA);
                }
            }
        }
    }

    private static boolean isPalindrome(long number) {
        long palindrome = number;
        long reverse = 0;

        while (palindrome != 0) {
            long remainder = palindrome % 10;
            reverse = reverse * 10 + remainder;
            palindrome = palindrome / 10;
        }
        if (number == reverse) {
            return true;
        }
        return false;
    }

    private void addPrimeNumberToList(long primeNumber) {
        primeList.add(primeNumber);
    }

    private boolean isPrime(int n) {
        if (n % 2 == 0) return false;
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0)
                return false;
        }
        return true;
    }

    private void sortElements(ArrayList<Model> palindromList) {
        int i, min, j;
        long tempRes, tempFirst, tempSec;

        for (i = 0; i < palindromList.size() - 1; i++) {
            min = i;
            for (j = i + 1; j < palindromList.size(); j++) {
                if (palindromList.get(j).getResult() < palindromList.get(min).getResult()) {
                    min = j;
                }
                tempRes = palindromList.get(min).getResult();
                tempFirst = palindromList.get(min).getFirst_element();
                tempSec = palindromList.get(min).getSecond_element();

                palindromList.get(min).setResult(palindromList.get(i).getResult());
                palindromList.get(min).setFirst_element(palindromList.get(i).getFirst_element());
                palindromList.get(min).setSecond_element(palindromList.get(i).getSecond_element());

                palindromList.get(i).setResult(tempRes);
                palindromList.get(i).setFirst_element(tempFirst);
                palindromList.get(i).setSecond_element(tempSec);
            }
        }
    }

    private void addPalindromNumberToList(long palindromNumber, long firstAttribute, long secondAttribute) {
        palindromList.add(new Model(palindromNumber, firstAttribute, secondAttribute));
    }

    private void showPalindrom() {
        System.out.println("Result: " + takeTheResult());
        System.out.println("First attribute: " + takeTheFirstAttribute());
        System.out.println("Second attribute: " + takeTheSecondAttribute());
    }

    private long takeTheResult() {
        return palindromList.get(palindromList.size() - 1).getResult();
    }

    private long takeTheFirstAttribute() {
        return palindromList.get(palindromList.size() - 1).getFirst_element();
    }

    private long takeTheSecondAttribute() {
        return palindromList.get(palindromList.size() - 1).getSecond_element();
    }

}
